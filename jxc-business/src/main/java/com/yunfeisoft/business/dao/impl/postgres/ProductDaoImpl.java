package com.yunfeisoft.business.dao.impl.postgres;

import com.applet.base.ServiceDaoImpl;
import com.applet.sql.builder.SelectBuilder;
import com.applet.sql.builder.WhereBuilder;
import com.applet.utils.Page;
import com.yunfeisoft.business.dao.inter.ProductDao;
import com.yunfeisoft.business.model.Product;
import com.yunfeisoft.business.model.ProductCategory;
import com.yunfeisoft.business.model.Supplier;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * ClassName: ProductDaoImpl
 * Description: 商品信息Dao实现
 * Author: Jackie liu
 * Date: 2020-07-23
 */
@Repository
public class ProductDaoImpl extends ServiceDaoImpl<Product, String> implements ProductDao {

    @Override
    public Page<Product> queryPage(Map<String, Object> params) {
        WhereBuilder wb = new WhereBuilder();
        wb.setOrderByWithDesc("p.createTime");
        if (params != null) {
            initPageParam(wb, params);
            wb.andEquals("p.orgId", params.get("orgId"));
            wb.andFullLike("p.name", params.get("name"));

            String namePinyin = (String) params.get("namePinyin");
            if (StringUtils.isNotBlank(namePinyin)) {
                wb.andGroup()
                        .orFullLike("p.name", namePinyin)
                        .orFullLike("p.pinyinCode", StringUtils.upperCase(namePinyin))
                        .orFullLike("p.code", namePinyin)
                        .andGroup();
            }
        }

        SelectBuilder builder = getSelectBuilder("p");
        builder.column("pc.name as categoryName")
                .column("s.name as supplierName")
                .leftJoin(ProductCategory.class).alias("pc").on("p.categoryId = pc.id").build()
                .leftJoin(Supplier.class).alias("s").on("p.supplierId = s.id").build();

        return queryPage(builder.getSql(), wb);
    }

    @Override
    public List<Product> queryList(Map<String, Object> params) {
        WhereBuilder wb = new WhereBuilder();
        if (params != null) {
            wb.andEquals("p.orgId", params.get("orgId"));
            wb.andFullLike("p.name", params.get("name"));
        }

        SelectBuilder builder = getSelectBuilder("p");
        builder.column("pc.name as categoryName")
                .column("s.name as supplierName")
                .join(ProductCategory.class).alias("pc").on("p.categoryId = pc.id").build()
                .join(Supplier.class).alias("s").on("p.supplierId = s.id").build();

        return query(builder.getSql(), wb);
    }

    @Override
    public boolean isDupName(String orgId, String id, String name) {
        WhereBuilder wb = new WhereBuilder();
        wb.andEquals("isDel", 2);
        wb.andEquals("orgId", orgId);
        wb.andEquals("name", name);
        wb.andNotEquals("id", id);
        return this.count(wb) > 0;
    }

    @Override
    public boolean isDupCode(String orgId, String id, String code) {
        WhereBuilder wb = new WhereBuilder();
        wb.andEquals("isDel", 2);
        wb.andEquals("orgId", orgId);
        wb.andEquals("code", code);
        wb.andNotEquals("id", id);
        return this.count(wb) > 0;
    }
}