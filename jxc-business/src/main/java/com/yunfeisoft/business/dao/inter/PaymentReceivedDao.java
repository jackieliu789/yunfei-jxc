package com.yunfeisoft.business.dao.inter;

import com.applet.base.BaseDao;
import com.applet.utils.Page;
import com.yunfeisoft.business.model.PaymentReceived;

import java.util.Map;

/**
 * ClassName: PaymentReceivedDao
 * Description: 收付款信息Dao
 * Author: Jackie liu
 * Date: 2020-08-11
 */
public interface PaymentReceivedDao extends BaseDao<PaymentReceived, String> {

    public Page<PaymentReceived> queryPage(Map<String, Object> params);
}