package com.yunfeisoft.business.model;

import com.applet.base.ServiceModel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * ClassName: WarehouseUser
 * Description: 仓库用户信息
 *
 * @Author: Jackie liu
 * Date: 2020-08-04
 */
@Entity
@Table(name = "TT_WAREHOUSE_USER")
public class WarehouseUser extends ServiceModel implements Serializable {

    /**
     * Field serialVersionUID: 序列号
     */
    private static final long serialVersionUID = 1L;

    /**
     * 组织id
     */
    @Column
    private String userId;

    /**
     * 仓库id
     */
    @Column
    private String warehouseId;


    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(String warehouseId) {
        this.warehouseId = warehouseId;
    }


}