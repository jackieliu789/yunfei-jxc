package com.yunfeisoft.business.service.impl;

import com.applet.base.BaseServiceImpl;
import com.yunfeisoft.business.dao.inter.CustomerDao;
import com.yunfeisoft.business.model.Customer;
import com.yunfeisoft.business.service.inter.CustomerService;
import com.applet.sql.separation.DataSourceChange;
import com.applet.utils.Page;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * ClassName: CustomerServiceImpl
 * Description: 客户信息service实现
 * Author: Jackie liu
 * Date: 2020-07-23
 */
@Service("customerService")
public class CustomerServiceImpl extends BaseServiceImpl<Customer, String, CustomerDao> implements CustomerService {

    @Override
    @DataSourceChange(slave = true)
    public Page<Customer> queryPage(Map<String, Object> params) {
        return getDao().queryPage(params);
    }

    @Override
    public List<Customer> queryList(Map<String, Object> params) {
        return getDao().queryList(params);
    }

    @Override
    public boolean isDupName(String orgId, String id, String name) {
        return getDao().isDupName(orgId, id, name);
    }
}