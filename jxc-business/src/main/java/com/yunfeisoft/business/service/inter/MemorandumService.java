package com.yunfeisoft.business.service.inter;

import com.applet.base.BaseService;
import com.yunfeisoft.business.model.Memorandum;
import com.applet.utils.Page;

import java.util.Map;

/**
 * ClassName: MemorandumService
 * Description: 备忘录信息service接口
 * Author: Jackie liu
 * Date: 2020-08-23
 */
public interface MemorandumService extends BaseService<Memorandum, String> {

    public Page<Memorandum> queryPage(Map<String, Object> params);
}