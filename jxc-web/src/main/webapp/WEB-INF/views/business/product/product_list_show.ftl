<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>商品列表</title>
	<#include "/common/vue_resource.ftl">
    <style>
        .select{background-color:#1E9FFF !important;color:#fff;}
    </style>
</head>
<body>
<div id="app" v-cloak>
    <div class="app-container" @click="hideMenu">
        <div class="layui-row app-header">
            <#--<div class="layui-col-md6">
                <div class="layui-btn-group">
                    <button type="button" class="layui-btn layui-btn-sm" @click="add">创建商品</button>
                    <button type="button" class="layui-btn layui-btn-sm" @click="exportExcel">导出Excel</button>
                    <button type="button" class="layui-btn layui-btn-sm" @click="importExcel">导入Excel</button>
                    <a class="layui-btn layui-btn-sm" href="${params.contextPath}/excel/商品信息_模板.xlsx">下载Excel模板</a>
                </div>
            </div>-->
            <div class="layui-col-md12 text-right">
                <input type="text" v-model="params.namePinyin" placeholder="商品名称" class="layui-input" @keyup.13="seachData"/>
                <button type="button" class="layui-btn layui-btn-sm layui-btn-primary search-button" @click="seachData">查询</button>
            </div>
        </div>
        <div class="app-list">
            <div class="layui-row">
                <div class="layui-col-md9">
                    <div class="app-table-num"><span class="num">商品列表(共 {{total}} 条)</span></div>
                </div>
                <div class="layui-col-md3 text-right">
                    <span class="prev" @click="loadPrev">上一页</span>
                    <span class="next" @click="loadNext">下一页</span>
                </div>
            </div>
            <table class="layui-table" lay-even lay-skin="nob" lay-size1="sm">
                <thead>
                <tr>
                    <th style="width:20px;">#</th>
                    <th>编码</th>
                    <th>名称</th>
                    <th>规格</th>
                    <th>类别</th>
                    <th>供应商</th>
                    <th>单位</th>
                    <@auth code='purchaseOrder_column_price'>
                        <th>最近进货价</th>
                        <th>平均进货价</th>
                    </@auth>
                    <th>拼音简码</th>
                    <#--<th style="width:125px;">操作</th>-->
                </tr>
                </thead>
                <tbody>
                <tr v-for="(item, index) in rows" style="cursor:pointer;" :class="selectIndex == index ? 'select' : ''" @click="selectProduct(index)">
                    <td>{{20 * (params.page - 1) + 1 + index}}</td>
                    <td>{{item.code}}</td>
                    <td>{{item.name}}</td>
                    <td>{{item.standard}}</td>
                    <td>{{item.categoryName}}</td>
                    <td>{{item.supplierName}}</td>
                    <td>{{item.unit}}</td>
                    <@auth code='purchaseOrder_column_price'>
                        <td>￥ {{item.buyPrice}}</td>
                        <td>￥ {{item.avgPrice}}</td>
                    </@auth>
                    <td>{{item.pinyinCode}}</td>
                    <#--<td class="more-parent">
                        <div class="ui-operating" @click="modify(index)">编辑</div>
                        <div class="ui-split"></div>
                        <div class="ui-operating" @click="showDetail(index)">查看</div>
                        <div class="ui-split"></div>
                        <div class="ui-operating" @click.stop="showMenu(index)">更多</div>
                        <div class="more-container" v-if="item.showMenu">
                            <div class="more-item" @click="remove(index)">删除</div>
                        </div>
                    </td>-->
                </tr>
                <tr v-if="rows.length <= 0">
                    <td colspan="11" class="text-center">没有更多数据了...</td>
                </tr>
                </tbody>
            </table>
            <div class="layui-row">
                <div class="layui-col-md6">
                    <div class="app-table-num"><span class="num">共 {{total}} 条</span></div>
                </div>
                <div class="layui-col-md6 text-right">
                    <span class="prev" @click="loadPrev">上一页</span>
                    <span class="next" @click="loadNext">下一页</span>
                </div>
            </div>
        </div>
    </div>

</div>
<script>
    var app = new Vue({
        el: '#app',
        data: {
            params: {
                namePinyin: '',
                page: 1,
            },
            rows: [],
            total: 0,
            selectIndex: '-1',
        },
        mounted: function () {
            this.loadData();
        },
        methods: {
            seachData:function(){
                this.params.page = 1;
                this.$nextTick(function () {
                    this.loadData();
                });
            },
            loadData: function () {
                var that = this;
                $.http.post("${params.contextPath}/web/product/list.json", this.params).then(function (data) {
                    if (!data.success) {
                        $.message(data.message);
                        return;
                    }
                    that.rows = data.rows;
                    that.total = data.total;
                });
            },
            loadNext: function () {
                if (this.rows.length < 20 && this.rows.length > 0) {
                    return;
                }
                this.params.page = this.params.page + 1;
                this.loadData();
            },
            loadPrev: function () {
                if (this.params.page <= 1) {
                    return;
                }
                this.params.page = this.params.page - 1;
                this.loadData();
            },
            showMenu:function (index) {
                this.hideMenu();
                this.$set(this.rows[index], "showMenu", true);
            },
            hideMenu: function () {
                var that = this;
                this.rows.forEach(function (item) {
                    that.$set(item, 'showMenu', false);
                });
            },
            selectProduct: function (index) {
                this.selectIndex = index;
                parent.app.selectProduct(this.rows[index] || {});
            }
        }
    });
</script>
</body>

</html>
