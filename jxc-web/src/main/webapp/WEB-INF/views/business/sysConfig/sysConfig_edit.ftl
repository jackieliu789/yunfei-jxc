<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>编辑系统配置</title>
    <#include "/common/vue_resource.ftl">
    <#include "/common/upload.ftl">
    <style>
        .anchor-point > a {
            color: #FF5722;
            margin-right: 5px;
        }

        .button-container {
            position: fixed;
            top: 0px;
            right: 0;
            width: 100%;
            border-bottom: 1px solid #eee;
            background: #fff;
            z-index: 10;
            line-height: 45px;
        }

        .images {
            display: inline-block;
            margin-left: 10px;
            vertical-align: top;
        }

        .images img {
            height: 30px;
            cursor: pointer;
            border-radius: 5px;
        }

        .images .remove {
            font-size: 12px;
            text-align: center;
            background-color: rgba(0, 0, 0, 0.6);
            color: #fff;
            display: inline-block;
            padding: 0px 10px;
            height: 30px;
            line-height: 30px;
            vertical-align: top;
            border-radius: 10%;
            cursor: pointer;
            margin-left: 5px;
        }
    </style>
</head>
<body>
<div id="app" v-cloak>
    <div class="layui-container" style="margin-top:60px;margin-bottom:20px;">
        <form class="layui-form ajax-form" @submit.prevent="submitForm()" method="post">
            <div class="button-container">
                <div class="layui-row">
                    <div class="layui-col-md9 layui-col-md-offset1 anchor-point">
                        导航：
                        <a href="#website-config">网站设置</a>
                        <#--<a href="#core-config">核心设置</a>
                        <a href="#attachment-config">附件设置</a>
                        <a href="#watermark-config">图片水印</a>
                        <a href="#leaflet-config">宣传页配置</a>-->
                        <a href="#interface-config">音视频配置</a>
                        <#--<a href="#board-config">白板配置</a>
                        <a href="#oss-config">云存储配置</a>-->
                    </div>
                    <div class="layui-col-md2">
                        <input type="submit" value="保存" class="layui-btn"/>
                    </div>
                </div>
            </div>
            <div class="layui-card" id="website-config">
                <div class="layui-card-header">网站设置</div>
                <div class="layui-card-body">
                    <div class="layui-form-item">
                        <label class="layui-form-label">网站全称<span class="ui-request">*</span></label>
                        <div class="layui-input-block">
                            <input type="text" v-model="record.websitName" class="layui-input"/>
                            <div class="layui-form-mid layui-word-aux">一般不超过10个字符</div>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">网站简称<span class="ui-request">*</span></label>
                        <div class="layui-input-block">
                            <input type="text" v-model="record.websitShortName" class="layui-input"/>
                            <div class="layui-form-mid layui-word-aux">一般不超过7个字符</div>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">网站LOGO<span class="ui-request">*</span></label>
                        <div class="layui-input-block">
                            <div style="display:inline-block;vertical-align:top;">
                                <img v-if="record.logoImagePath"
                                     :src="'${params.fileRequestUrl!}' + record.logoImagePath"
                                     style="max-height:150px;max-width:150px;">
                            </div>
                            <div style="display:inline-block;vertical-align:top;">
                                <a id="select-logo-button">选择图片</a>
                                <div class="layui-form-mid layui-word-aux">JPG、PNG格式，图片小于5M</div>
                            </div>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">地址栏图标</label>
                        <div class="layui-input-block">
                            <div style="display:inline-block;vertical-align:top;">
                                <img v-if="record.faviconImagePath"
                                     :src="'${params.fileRequestUrl!}' + record.faviconImagePath"
                                     style="max-height:150px;max-width:150px;">
                            </div>
                            <div style="display:inline-block;vertical-align:top;">
                                <a id="select-favicon-button">选择图片</a>
                                <div class="layui-form-mid layui-word-aux">建议尺寸：32像素×32像素。必须是ico文件</div>
                            </div>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">版权信息</label>
                        <div class="layui-input-block">
                            <textarea v-model="record.copyright" placeholder="请输入版权信息" class="layui-textarea"></textarea>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">备案号</label>
                        <div class="layui-input-block">
                            <input type="text" v-model="record.recordNumber" placeholder="请输入备案号" class="layui-input"/>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">账号有效期</label>
                        <div class="layui-input-block">
                            <input type="text" v-model="record.newAccountValidity" placeholder="新账号有效期(单位月)" class="layui-input"/>
                        </div>
                    </div>
                    <#--end-->
                </div>
            </div>

            <#--<div class="layui-card" id="interface-config">
                <div class="layui-card-header">音视频配置</div>
                <div class="layui-card-body">
                    <div class="layui-form-item">
                        <label class="layui-form-label">视频源</label>
                        <div class="layui-input-block" style="padding-top:10px;min-height: 26px;">
                            <label class="ui-form-label"><input type="radio" v-model="record.liveSource" value="1"/> 阿里云</label>
                            <label class="ui-form-label"><input type="radio" v-model="record.liveSource" value="2"/> 腾讯云</label>
                            <label class="ui-form-label"><input type="radio" v-model="record.liveSource" value="3"/> YY</label>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">域名</label>
                        <div class="layui-input-block">
                            <input type="text" v-model="record.liveDomain" class="layui-input" v-if="record.liveSource != 3"/>
                            <input type="text" v-model="record.liveDomain" class="layui-input" v-if="record.liveSource == 3" disabled/>
                            <div class="layui-form-mid layui-word-aux">视频源名称</div>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">AppName</label>
                        <div class="layui-input-block">
                            <input type="text" v-model="record.liveAppName" class="layui-input" v-if="record.liveSource != 3"/>
                            <input type="text" v-model="record.liveAppName" class="layui-input" v-if="record.liveSource == 3" disabled/>
                            <div class="layui-form-mid layui-word-aux">视频源名称，不能输入汉字，只能输入字母</div>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">鉴权Key</label>
                        <div class="layui-input-block">
                            <input type="text" v-model="record.liveAccessKey" class="layui-input" v-if="record.liveSource != 3"/>
                            <input type="text" v-model="record.liveAccessKey" class="layui-input" v-if="record.liveSource == 3" disabled/>
                            <div class="layui-form-mid layui-word-aux">视频源名称，不能输入汉字，只能输入字母</div>
                        </div>
                    </div>
                    &lt;#&ndash;end&ndash;&gt;
                </div>
            </div>-->

        </form>
    </div>
</div>
<script type="text/javascript">
    var app = new Vue({
        el: '#app',
        data: {
            showTypes: false,
            record: {
                websitName: '',
                websitShortName: '',
                logoImagePath: '',
                faviconImagePath: '',
                copyright: 'Copyright © 2019-2020 Yunfeisoft.com. 云飞CMS 版权所有',
                recordNumber: '',
                newAccountValidity: '',
            },
        },
        mounted: function () {
            this.init();
            this.loadData();
        },
        methods: {
            init: function () {
                var that = this;
                //网站LOGO
                $.upload({
                    renderId: "#select-logo-button",
                    accept: {title: 'Images', extensions: 'gif,jpg,jpeg,bmp,png', mimeTypes: 'image/*'},
                    uploadSuccess: function (file, data) {
                        if (!data.success) {
                            $.message(data.message);
                            return;
                        }
                        var result = data.data[0];
                        that.record.logoImagePath = result.path;
                    }
                });

                //地址栏图标
                $.upload({
                    renderId: "#select-favicon-button",
                    accept: {title: 'Images', extensions: 'ico', mimeTypes: 'image/x-icon'},
                    uploadSuccess: function (file, data) {
                        if (!data.success) {
                            $.message(data.message);
                            return;
                        }
                        var result = data.data[0];
                        that.record.faviconImagePath = result.path;
                    }
                });
            },
            loadData: function () {
                var that = this;
                $.http.post("${params.contextPath}/web/sysConfig/list.json").then(function (data) {
                    if (!data.success) {
                        $.message(data.message);
                        return;
                    }
                    var list = data.data;
                    for (var i = 0, size = list.length; i < size; i++) {
                        var record = list[i];
                        that.record[record.key] = record.value || "";
                    }
                });
            },
            submitForm: function () {
                $.http.post("${params.contextPath}/web/sysConfig/save.json", this.record).then(function (data) {
                    $.message(data.message);
                });
            },
        }
    });
</script>
</body>

</html>
