一：js默认校验规则

    (1)required:true           必输字段
    (2)remote:"check.php"      使用ajax方法调用check.php验证输入值
    (3)email:true              必须输入正确格式的电子邮件
    (4)url:true                必须输入正确格式的网址
    (5)date:true               必须输入正确格式的日期 日期校验ie6出错，慎用
    (6)dateISO:true            必须输入正确格式的日期(ISO)，例如：2009-06-23，1998/01/22 只验证格式，不验证有效性
    (7)number:true             必须输入合法的数字(负数，小数)
    (8)digits:true             必须输入整数
    (9)creditcard:             必须输入合法的信用卡号
    (10)equalTo:"#field"       输入值必须和#field相同a
    (11)accept:                输入拥有合法后缀名的字符串（上传文件的后缀）
    (12)maxlength:5            输入长度最多是5的字符串(汉字算一个字符)
    (13)minlength:10           输入长度最小是10的字符串(汉字算一个字符)
    (14)rangelength:[5,10]     输入长度必须介于 5 和 10 之间的字符串")(汉字算一个字符)
    (15)range:[5,10]           输入值必须介于 5 和 10 之间
    (16)max:5                  输入值不能大于5
    (17)min:10                 输入值不能小于10

    a) 使用方式：<input type="text" name="name" placeholder="请输入名称" class="layui-input {required:true,max:5}"/>

    b) 自定义校验规则：写在jquery.validate.methods.js文件中
       //检测手机号是否正确
       jQuery.validator.addMethod("isMobile", function(value, element) {
            var length = value.length;
            var regPhone = /^1([3578]\d|4[57])\d{8}$/;
            return this.optional(element) || ( length == 11 && regPhone.test( value ) );
       }, "请正确填写您的手机号码");


二：数据表命名规则：

	  TC_     通用代码表，例如TC_REC_TYPE  代表所有数据库表记录类型
	  TG_	  与外围系统交互的中间表，例如 TG_SAP_CONTRACT  代表来自SAP系统的表
	  TL_     日志表，各种日志信息记录，例如 TL_IT_LOG 记录用户访问信息记录
	  TM_	  企业内共享的主数据表，例如 TM_PART 代表 零件清单
	  TR_	  关联中间表	TR_USER_ROLE  记录用户和角色之间的关系
	  TS_     系统表，例如 TS_MENU	菜单信息  TS_USER 用户信息
	  TT_	  业务数据表，TT_RFQ	询报价记录
	  TE_	  临时表，仅在导入数据或生成报表时使用


三：字典组件使用

    1. 默认初始化方式：
       <select name="level" class="layui-select dic-select" dic-code="hospitalLevel"></select>

    2. 自定义初始化方式：
        var options = DictionaryHelper.assemble({
             dictionaryValue:'字典值',
             domType:'装配类型，取值为：option,checkbox,radio',
             domName:'dom元素名称',
             domClass:'dom元素class名称',
             domOther:'dom元素其他属性',
             selfAssemble:function(dictionaryItems){}//自定义装配函数，定义此项则domType和domName失效'
        });
        obj.html(options);

     3. 获取数据字典操作对象：var dictionaryOperator = DictionaryHelper.getDictionaryOperator(dictionaryValue);
        获取字典项名称：DictionaryOperator.getDictionaryItemName(dictionaryItemValue);
        获取字典项对象：DictionaryOperator.getDictionaryItem(dictionaryItemValue);


四：字典说明

    字典名称                         字典值

    一级科室                         topHospitalOffice
    
五：省市区联动组件使用

    1. 初始化：
        <div class="layui-input-block ui-area area-select">
            <select class="layui-select"></select>
            <select class="layui-select"></select>
            <select class="layui-select"></select>
            <input type="hidden" name="areas" value=""/>
        </div>

        或

        var areas = [{"id":"1191822504331862019","name":"安徽省"},{"id":"1191822504331862020","name":"合肥市"},{"id":"1191822504331862021","name":"蜀山区"}];
        AreaHelper.initPage({domObj: $(".area-select"), data: areas});

六：富单选组件使用

    1. 注册数据处理监听器，每个模块的监控handlerId需不同：

        LocalStorage.registHandler("handlerId", function (data) {
            $('input[name="hospitalId"]').val(data.id);
            $('input[name="hospitalName"]').val(data.name);
        });

    2. 初始化dom元素：

        <div class="layui-form-item">
            <label class="layui-form-label">所属医院<span class="ui-request">*</span></label>
            <div class="layui-input-block ui-more-parent">
                <input type="hidden" name="hospitalId"/>
                <input type="text" name="hospitalName" placeholder="请选择所属医院" p="model:'hospital',handlerId:'handlerId'" class="layui-input single-model-select {required:true}" readonly/>
            </div>
        </div>


七：生成代码xml格式

    说明：
        1，如果需要设计登录账号的模块，登录账号统一使用系统user表
        2，涉及图片的模块使用附件表存储
        3，涉及大字符字段的模块用data表存储，比如模块中含有大文本字段
        4，主键字段用bigint类型
        5，枚举数字字段用int2类型

    <root>
    	<tables>

    		<table name="tt_institution" comment="机构信息" module="web" engine="InnoDB" charset="utf8">
    			<field name="id" comment="主键" primary="true" type="bigint"/>

    			<field name="name" comment="名称" type="varchar" length="50"/>
    			<field name="idcard" comment="身份证" type="varchar" length="20"/>
    			<field name="sex" comment="性别" type="int2"/>
    			<field name="linkPhone" comment="联系电话" type="varchar" length="20"/>
    			<field name="nation" comment="民族(字典域)" type="varchar" length="20"/>
    			<field name="birthDate" comment="出生年月" type="varchar" length="20"/>
    			<field name="degree" comment="文化程度(字典域)" type="varchar" length="20"/>
    			<field name="areas" comment="区域(json)" type="varchar" length="200"/>
    			<field name="address" comment="联系地址" type="varchar" length="100"/>
    			<field name="professionType" comment="职业类型(字典域)" type="varchar" length="20"/>
    			<field name="professionTypeOther" comment="其他职业类型" type="varchar" length="50"/>
    			<field name="registerType" comment="注册方式(字典域)" type="varchar" length="20"/>
    			<field name="serviceSkill" comment="服务技能" type="varchar" length="500"/>
    			<field name="institutionId" comment="所属中心/驿站(机构id)" type="bigint"/>
    			<field name="intro" comment="个人说明" type="varchar" length="500"/>
    			<field name="state" comment="认证状态(1待认证，2认证通过，3认证不通过)" type="int2"/>
    			<field name="reason" comment="认证不通过原因" type="varchar" length="200"/>
    			<field name="beginTime" comment="服务开始时间(HH:mm)" type="varchar" length="10"/>
    			<field name="endTime" comment="服务结束时间(HH:mm)" type="varchar" length="10"/>

    			<field name="createId" comment="创建人id" type="bigint"/>
                <field name="createTime" comment="创建时间" type="timestamp"/>
                <field name="modifyId" comment="修改人id" type="bigint"/>
                <field name="modifyTime" comment="修改时间" type="timestamp"/>

    		</table>

    	</tables>
    </root>
